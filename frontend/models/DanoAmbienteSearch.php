<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\DanoAmbiente;

/**
 * DanoAmbienteSearch represents the model behind the search form about `\frontend\models\DanoAmbiente`.
 */
class DanoAmbienteSearch extends DanoAmbiente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id_dano_ambiente', 'Reserva_id_reserva'], 'integer'],
            [['Detalle_del_dano', 'Tipo_de_dano'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAdmin($params)
    {
        $query = DanoAmbiente::find()->innerJoinWith("reserva a");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id_dano_ambiente' => $this->Id_dano_ambiente,
            'Reserva_id_reserva' => $this->Reserva_id_reserva,
        ]);

        $query->andFilterWhere(['like', 'Detalle_del_dano', $this->Detalle_del_dano])
            ->andFilterWhere(['like', 'Tipo_de_dano', $this->Tipo_de_dano]);

        return $dataProvider;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DanoAmbiente::find()->innerJoinWith("reserva a");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id_dano_ambiente' => $this->Id_dano_ambiente,
            'Reserva_id_reserva' => $this->Reserva_id_reserva,
            'a.usuario_id' =>Yii::$app->user->id,
        ]);

        $query->andFilterWhere(['like', 'Detalle_del_dano', $this->Detalle_del_dano])
            ->andFilterWhere(['like', 'Tipo_de_dano', $this->Tipo_de_dano]);

        return $dataProvider;
    }
}
