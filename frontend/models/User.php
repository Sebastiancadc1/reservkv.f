<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id ID
 * @property string $id_number Número de identificación del usuario
 * @property string $name Nombres
 * @property string $surname Apellidos
 * @property string $username Nombre de usuario
 * @property string|null $email Correo personal
 * @property int $estado ¿Está activo?
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $verification_token
 * @property int|null $creado_por Creado por
 * @property string $created_at Creado el
 * @property int|null $actualizado_por Actualizado por
 * @property string $updated_at Modificado el
 *
 * @property Reserva[] $reservas
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_number', 'name', 'surname', 'username', 'estado', 'auth_key', 'password_hash', 'verification_token', 'created_at', 'updated_at'], 'required'],
            [['estado', 'creado_por', 'actualizado_por'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id_number'], 'number'],
            [['name', 'surname', 'email'], 'string', 'max' => 100],
            [['username'], 'string'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'verification_token'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['id_number'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_number' => 'Cédula',
            'name' => 'Nombre',
            'surname' => 'Apellido',
            'username' => 'Usuario',
            'email' => 'Email',
            'estado' => 'Estado',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'verification_token' => 'Verification Token',
            'creado_por' => 'Creado Por',
            'created_at' => 'Created At',
            'actualizado_por' => 'Actualizado Por',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Reservas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['usuario_id' => 'id']);
    }
}