<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Ambiente;

/**
 * AmbienteSearch represents the model behind the search form of `\frontend\models\Ambiente`.
 */
class AmbienteSearch extends Ambiente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ambiente'], 'integer'],
            [['title', 'objetos_del_ambiente', 'cantidad_de_objetos', 'capacidad', 'Lugar', 'color'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ambiente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_ambiente' => $this->id_ambiente,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'objetos_del_ambiente', $this->objetos_del_ambiente])
            ->andFilterWhere(['like', 'cantidad_de_objetos', $this->cantidad_de_objetos])
            ->andFilterWhere(['like', 'capacidad', $this->capacidad])
            ->andFilterWhere(['like', 'Lugar', $this->Lugar])
            ->andFilterWhere(['like', 'color', $this->color]);

        return $dataProvider;
    }
}
