<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ambiente".
 *
 * @property int $id_ambiente
 * @property string $title
 * @property string $objetos_del_ambiente
 * @property string $cantidad_de_objetos
 * @property string $capacidad
 * @property string $Lugar
 * @property string $color
 *
 * @property DanoAmbiente[] $danoAmbientes
 * @property Reserva[] $reservaIdReservas
 * @property Reserva[] $reservas
 */
class Ambiente extends \yii\db\ActiveRecord
{

    public $title2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ambiente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'objetos_del_ambiente', 'cantidad_de_objetos', 'capacidad', 'Lugar', 'color'], 'required'],
            [['title'], 'string', 'max' => 14],
            [['objetos_del_ambiente'], 'string', 'max' => 45],
            [['cantidad_de_objetos'], 'string', 'max' => 30],
            [['capacidad'], 'string', 'max' => 6],
            [['Lugar', 'color'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ambiente' => 'Id Ambiente',
            'title' => 'Title',
            'objetos_del_ambiente' => 'Objetos Del Ambiente',
            'cantidad_de_objetos' => 'Cantidad De Objetos',
            'capacidad' => 'Capacidad',
            'Lugar' => 'Lugar',
            'color' => 'Color',
        ];
    }

    /**
     * Gets query for [[DanoAmbientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanoAmbientes()
    {
        return $this->hasMany(DanoAmbiente::className(), ['title' => 'id_ambiente']);
    }

    /**
     * Gets query for [[ReservaIdReservas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReservaIdReservas()
    {
        return $this->hasMany(Reserva::className(), ['id' => 'Reserva_id_reserva'])->viaTable('dano_ambiente', ['title' => 'id_ambiente']);
    }

    /**
     * Gets query for [[Reservas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['title' => 'id_ambiente']);
    }
}
