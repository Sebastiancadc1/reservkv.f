<?php

use frontend\models\Reserva;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reserva */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Reservas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$id     = $_GET['id'];
$reserva = Reserva::findOne($id);
$titulo = $reserva->ambiente->title;
$evento = $reserva->body;
$tipo   = $reserva->type_event;
$status = $reserva->status;
$usuario = $reserva->usuario->name . ' ' . $reserva->usuario->surname;
$inicio = DateTime::createFromFormat("Y-m-d H:i:s", $reserva->inicio_normal)->format("d/m/Y H:i");
$final  = DateTime::createFromFormat("Y-m-d H:i:s", $reserva->final_normal)->format("d/m/Y H:i");

?>
<head>
<title>RESERVK | Información  del evento</title>


<script language="JavaScript">
  function redireccionar() {
    setTimeout('document.location.reload()',2000);
  }
  </script>
</head>

<div class="reserva-view box box-primary">
    <div class="polo">

        <ul>
            <li role="presentation" class="active">
                <a aria-controls="flights" role="tab" data-toggle="tab">
                    <h3>
                    Información  del evento
                    </h3>
                </a>
            </li>

            <form action="../reserva/update?id=<?= $_GET['id'] ?>" method="post" id="event">
                <input type="hidden" class="type_e" name="type_e" value="">
                <input type="hidden" name="tipo" value="<?= $tipo ?>">
                <div class="">

                    <table class="table table-bordered active" id="customers">
                        <tr>
                            <th>Ambiente:</th>
                            <th>Evento:</th>
                            <th>Usuario:</th>
                        </tr>

                        <tr>
                            <td><?= $titulo ?></td>
                            <td> <?= $evento ?></td>
                            <td> <?= $usuario ?></td>
                        </tr>

                    </table>
                    <div class="rows">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="email">El evento empieza el: </label>
                                <div class='input-group date' id='from'>
                                    <input type='text' id="from" name="from" class="form-control dates" readonly value="<?= $inicio ?>" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>

                        </div><br>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="email">El evento terminar el: </label>
                                <div class='input-group date' id='to'>
                                    <input type='text' id="to" name="to" class="form-control dates" readonly value="<?= $final ?>" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="alert alert-danger"  role="alert">

                </div>
                <div class="alert alert-success"  role="alert">
                </div>

                <div class="row pad">
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-primary" id="modificar" name="modificar_evento" <?= ($status == 'cancelado') ? 'disabled' : null; ?>>Modificar</button>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-danger" id="eliminar" name="eliminar_evento" <?= ($status == 'cancelado') ? 'disabled' : null; ?> onClick="redireccionar()" >Cancelar</button>
                    </div>
                    <div class="col-md-1">
                        <a type="button" href="./" class="btn btn-success">Volver</a>
                    </div>
                </div>
            </form>


            </center>
    </div>

</div>
<script>
        var url_ = "<?= Yii::$app->request->baseUrl . '/..'; ?>";
    </script>
    <?php
    $this->registerJsFile(
        '@web/../assets/jscalendario/jquery.min.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
    $this->registerJsFile(
        '@web/../assets/js/calendario.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );

    ?>