<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\DanoAmbiente */
?>
<div class="dano-ambiente-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'Id_dano_ambiente',
            'Reserva_id_reserva',
            'Tipo_de_dano',
            'Detalle_del_dano',
            
        ],
    ]) ?>

</div>
