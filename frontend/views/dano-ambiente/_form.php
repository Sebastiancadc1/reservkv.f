<?php

use frontend\models\Ambiente;
use frontend\models\Reserva;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\models\DanoAmbiente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dano-ambiente-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'Reserva_id_reserva')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Reserva::find()->innerJoinWith("ambiente2 a")->select
    (["reserva.*","CONCAT(reserva.id,' - ',a.title) as title2"])
    ->where(['usuario_id'=>Yii::$app->user->id])->asArray()->all(), 'id', 'title2'),
    'options' => ['placeholder' => 'Seleccione un ambiente'],
    'pluginOptions' => [
        'allowClear' => true
    ],]);?>
    <?= $form->field($model, 'Tipo_de_dano')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'Detalle_del_dano')->textarea(['maxlength' => 200]) ?>
   

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>