<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DanoAmbiente */

?>
<div class="dano-ambiente-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
