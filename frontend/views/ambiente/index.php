<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AmbienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ambientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ambiente-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Ambiente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_ambiente',
            'title',
            'objetos_del_ambiente',
            'cantidad_de_objetos',
            'capacidad',
            //'Lugar',
            //'color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
