<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\AmbienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ambiente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_ambiente') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'objetos_del_ambiente') ?>

    <?= $form->field($model, 'cantidad_de_objetos') ?>

    <?= $form->field($model, 'capacidad') ?>

    <?php // echo $form->field($model, 'Lugar') ?>

    <?php // echo $form->field($model, 'color') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
