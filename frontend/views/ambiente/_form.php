<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ambiente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ambiente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'objetos_del_ambiente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad_de_objetos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'capacidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lugar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
