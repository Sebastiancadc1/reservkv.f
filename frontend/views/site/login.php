<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use yii\widgets\Breadcrumbs;

$this->title = 'Reservk | Inicio de Sesión';
?>
<head>
     <link rel="shortcut icon" href="../../assets/images/favicon.png">
	<link rel="stylesheet" href="../../assets/styles/login2.css">
</head>


	<section>
		<div class="container" id="container">
			<!-- <div class="form-container sign-up-container">
					</div> -->
			<div class="form-container sign-in-container">
				<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

				<div> <input type="image" src="../../assets/images/logo-footer.png" class="imgh"> </div>

				<h1>Ingresar</h1>

				<?= $form->field($model, 'username')->textInput(['id' => 'username', 'autofocus' => true, 'class' => 'si', 'maxlength' => '28', 'minlength' => '1']) ?>

				<?= $form->field($model, 'password')->passwordInput(['id' => 'txtPassword', 'class' => 'sis', 'maxlength' => '18', 'minlength' => '1']) ?>
				<button class="ojo" onclick="mostrarPassword()"> <div style="width: 24px;"><img src="../../assets/images/ojo(1).png" style="height: 24px;  width: 31px;"></div></button>
				
				<span>              <br>
			 </span>
				
				<?= Html::a('¿Olvidaste tu contraseña?', ['site/request-password-reset']) ?>
				 <!-- ¿Necesitas una nueva verificación? <?= Html::a('Reenviar', ['site/resend-verification-email']) ?>  -->
				<?= Html::submitButton('Ingresar', ['id' => 'btn','class' => 'buton', 'name' => 'login-button']) ?>
				<a class="but" href="<?= Url::to(['site/']) ?>">Cancelar</a>
				<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>¡Cuidado!</strong> Es muy importante que leas este mensaje de alerta.
</div>

<div>
<?= Breadcrumbs::widget([
'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= Alert::widget() ?>
</div>

				<?php ActiveForm::end(); ?>

			</div>
			<div class="overlay-container">
				<div class="overlay">
					<div class="overlay-panel overlay-left">
						<h1>S</h1>
						<p>Inicie sesión con su información personal</p>
						<button class="ghost" id="signIn" onclick="btns()">Iniciar sesión</button>
					</div>
					<div class="overlay-panel overlay-right">
					<img src="../../assets/images/Logo-blanco.png">
						<h1>Hola, amigo!</h1>
						<p>Registrese con Reservk y comience sus reservas con nosotros.</p>

						<button onclick="location.href='<?= Url::to(['site/signup']) ?>'" class="ghost"><b>Registrarse</b></button>
					</div>

				</div>
			</div>
		</div>
	</section>
	<footer>

		<div class="row footer_row">

			<div class="foot" class="copyright">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;
				<script>
					document.write(new Date().getFullYear());
				</script> Reservk Todos los derechos reservados.
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</div>
		</div>
		<input type="image" src="../../assets/images/logo-footer.png" class="imgfoot">
		</div>
	</footer>
