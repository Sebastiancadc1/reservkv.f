<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reservk | Recuperar Contraseña';
?>

<head>
	<link rel="stylesheet" href="../../assets/styles/olvicontra.css">

</head>

	<section>
		<div class="container" id="container">
			<div class="form-container sign-up-container">

			</div>
			<div class="form-container sign-in-container">
				<?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
				<div> <input type="image" src="../../assets/images/logo-footer.png" class="imgh"> </div>

				<h1>Recuperar contraseña</h1>

				<?= $form->field($model, 'email')->textInput(['id' => 'email', 'autofocus' => true, 'class' => 'si', 'maxlength' => '30', 'minlength' => '10']) ?>

				<div class="form-group">
					<?= Html::submitButton('Enviar', ['class' => 'si']) ?>
				</div>
				<!-- <button  class="but"> <a href="<?= Url::to(['site/']) ?>"></a>Cancelar</button> -->
				<a class="but" href="<?= Url::to(['site/']) ?>">Cancelar</a>
				<?php ActiveForm::end(); ?>
			</div>



			<div class="overlay-container">
				<div class="overlay">

					<div class="overlay-panel overlay-right">
						<img src="../../assets/images/Logo-blanco.png">
						<h1>Hola, amigo!</h1>
						<p>Ingrese su correo y cambie su contraseña.</p>
					</div>

				</div>
			</div>
		</div>
	</section>


	<footer>

		<div class="row footer_row">

			<div class="fto" class="copyright">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;
				<script>
					document.write(new Date().getFullYear());
				</script> Reservk Todos los derechos reservados.
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</div>
		</div>
		<input type="image" src="../../assets/images/logo-footer.png" class="imgfooter">
		</div>

	</footer>