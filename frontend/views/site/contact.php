<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Reservk | Contáctanos';
?>
<div class="site-contact">
    <div class="home">
        <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/contact.jpg'; ?>)"></div>
        <div class="home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content text-center">
                            <div class="home_title" style="margin-top: -108px;">Contacto</div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        <div class="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact_content">
                            <div class="contact_title">
                                <h2>Ponerse en contacto</h2>
                            </div>
                            <div class="contact_list">
                                <ul>
                                    <li>Calle 52 No. 13-65, Bogotá, Colombia </li>
                                    <li>Tel.: +57 (1) 594 13 01 IP 17003</li>
                                    <li>ambientes.rev@gmail.com</li>
                                </ul>
                            </div>
                            <div class="contact_form_container">
                                <?php $form = ActiveForm::begin(['id' => 'contact_form', 'class' => 'contact_form']); ?>
                                <div class="row">
                                    <div class="col-md-6 input_container">
                                        <?= $form->field($model, 'name')->textInput(['id' => 'name','autofocus' => true, 'class' => 'contact_input', 'maxlength' => '20', 'minlength' => '5']) ?>
                                    </div>
                                    <div class="col-md-6 input_container">
                                        <?= $form->field($model, 'email')->textInput(['id' => 'email','class' => 'contact_input', 'maxlength' => '30', 'minlength' => '10']) ?>
                                    </div>
                                </div>
                                <div class="input_container">
                                    <?= $form->field($model, 'subject')->textInput(['id' => 'subject','class' => 'contact_input', 'maxlength' => '30', 'minlength' => '1']) ?>
                                </div>
                                <div class="input_container">
                                    <?= $form->field($model, 'body')->textarea(['id' => 'body','rows' => 6, 'class' => 'contact_input contact_textarea', 'maxlength' => '200', 'minlength' => '10']) ?>
                                </div>
                                <div class="input_container">
                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                    ]) ?> </div>
                                <button class="contact_button">Enviar</button>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>

                    <!-- Google Map -->
                    <div class="col-xl-5 col-lg-6 offset-xl-1">
                        <div class="contact_map">

                            <!-- Google Map -->
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1931.7691042281022!2d-74.06698665886438!3d4.638896033489767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9a3081295ccb%3A0xd4347f67e96f6d8b!2sSENA%20CGMLTI!5e0!3m2!1ses-419!2sco!4v1582176256398!5m2!1ses-419!2sco" style="border:0;margin-left: -48px;  ;margin-top: 24px; height: 791px;" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
</div>
<footer class="footer">
        <div class="footer_content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="footer_logo_container text-center">


                        </div>
                    </div>
                    <div class="row footer_row">
                    
                    <input type="image" src="../../assets/images/logo-footer.png" class="foots">
                        <div class="copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script> Reservk Todos los derechos reservados.
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->



                        </div>
    </footer>