<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use common\widgets\Alert;
use yii\widgets\Breadcrumbs;
$this->title = 'Reservk | Inicio';

?>
<head>
<link rel="shortcut icon" href="../assets/images/favicon.png">
</head>

<div class="super_container">
    <section>
        <div class="home">
            <div class="home_slider_container">
                <div class="owl-carousel owl-theme home_slider">

                    <!-- Slide -->
                    <div class="slide">
                        <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/Auditoriotarima.jpeg'; ?>)"></div>
                        <div class="home_container">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="home_content text-center">
                                            <div class="home_title">¿Necesita reservar los ambientes del Sena? Con Resevk podrá agendar de manera más rapida y efectiva.</div>
                                            <div class="booking_form_container">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide -->
                    <div class="slide">
                        <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/Auditoriotarima.jpeg'; ?>)"></div>
                        <div class="home_container">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="home_content text-center">
                                            <div class="home_title">¿Hubo algún daño en su reserva? Con Reservk usted podrá reportar las novedades de los ambientes.</div>
                                            <div class="booking_form_container">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide -->
                    <div class="slide">
                        <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/Auditoriotarima.jpeg'; ?>)"></div>
                        <div class="home_container">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="home_content text-center">
                                            <div class="home_title">¿Olvidó la fecha de su reserva? En Reservk usted podrá obtener un registro de sus reservas y los daños de los ambientes.</div>
                                            <div class="booking_form_container">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Features -->
        <div class="alerta">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                
            </div>
        <div class="features">
            <div class="container">
                <div class="row">

                    <!-- Icon Box -->
                    <div class="col-lg-4 icon_box_col">
                        <div class="icon_box d-flex flex-column align-items-center justify-content-start text-center">
                            <div class="icon_box_icon"><img src="<?= Yii::$app->request->baseUrl . '/../assets/images/pantalla-de-cine'; ?>.png"></div>
                            <div class="icon_box_title">
                                <h2>Reservas</h2>
                            </div>
                            <div class="icon_box_text">
                                <p style="color:#6d6d6d;">En Reservk usted podra la manejar las reservas de los siguientes ambientes: auditorio,audiovisuales y gimnasio</p>
                            </div>
                        </div>
                    </div>

                    <!-- Icon Box -->
                    <div class="col-lg-4 icon_box_col">
                        <div class="icon_box d-flex flex-column align-items-center justify-content-start text-center">
                            <div class="icon_box_icon"><img src="<?= Yii::$app->request->baseUrl . '/../assets/images/dano.png'; ?>" class="svg"></div>
                            <div class="icon_box_title">
                                <h2>Registro de daños </h2>
                            </div>
                            <div class="icon_box_text">
                                <p style="color:#6d6d6d;">En Reservk usted podrá registrar el daño que suceda mientras se está en el ambiente reservado.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Icon Box -->
                    <div class="col-lg-4 icon_box_col">
                        <div class="icon_box d-flex flex-column align-items-center justify-content-start text-center">
                            <div class="icon_box_icon"><img src="<?= Yii::$app->request->baseUrl . '/../assets/images/historia.png'; ?>" class="svg"></div>
                            <div class="icon_box_title">
                                <h2>Historial</h2>
                            </div>
                            <div class="icon_box_text">
                                <p style="color:#6d6d6d;">En Reservk podra ver sus reservas y tener un registro de daños generados en los ambientes y sus reportes.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Booking -->

        <div class="booking">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="booking_title text-center">
                            <h2>Reserva un ambiente</h2>
                        </div>
                        <!-- Booking Slider -->
                        <div class="booking_slider_container">
                            <div class="owl-carousel owl-theme booking_slider">

                                <!-- Slide -->
                                <div class="booking_item">
                                    <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/Audio11.png'; ?>)"></div>
                                    <div class="booking_overlay trans_200"></div>
                                    <div class="booking_item_content">
                                        <div class="booking_item_list">
                                            <ul>
                                                <li>Capacidad / 45 personas</li>
                                                <li>Elementos / parlantes,videovol</li>
                                                <li>Lugar / Segundo piso</li>
                                                <li>Descripcion / Ideal para presentaciones o charlas</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="booking_link"><a href="booking.html">Audio visuales</a> </div>
                                </div>

                                <!-- Slide -->
                                <div class="booking_item">
                                    <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/Aduditorio11.png'; ?>)"></div>
                                    <div class="booking_overlay trans_200"></div>
                                    <div class="booking_item_content">
                                        <div class="booking_item_list">
                                            <ul>
                                                <li>Capacidad / 210 personas </li>
                                                <li>Elementos / parlantes,videovol y cabina</li>
                                                <li>Lugar / Segundo piso</li>
                                                <li>Descripcion / Ideal para conferencias, presentaciones, exposiciones, eventos </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="booking_link"><a href="booking.html">Auditorio</a></div>
                                </div>

                                <!-- Slide -->
                                <div class="booking_item">
                                    <div class="background_image" style="background-image:url(<?= Yii::$app->request->baseUrl . '/../assets/images/maquinasgym22.jpeg'; ?>)"></div>
                                    <div class="booking_overlay trans_200"></div>
                                    <div class="booking_item_content">
                                        <div class="booking_item_list">
                                            <ul>
                                                <li>Capacidad / 35 personas </li>
                                                <li>Elementos / maquinas y elementos deportivos </li>
                                                <li>Lugar / Septimo piso</li>
                                                <li>Descripcion / Ideal para actividades físicas</li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="booking_link"><a href="booking.html">Gimnasio</a></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<footer class="footer">
        <div class="footer_content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="footer_logo_container text-center">


                        </div>
                    </div>
                    <div class="row footer_row">
                    
                    <input type="image" src="../assets/images/logo-footer.png" class="foots">
                        <div class="copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script> Reservk Todos los derechos reservados.
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->



                        </div>
    </footer>