<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<section>
<div class="site-error">

    <h1>!Acceso Denegado!</h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
    No tiene permiso para realizar esta acción.
    <a href="index.php">OK</a>
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>
</section>