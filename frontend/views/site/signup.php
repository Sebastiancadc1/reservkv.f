<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\widgets\Alert;
use yii\widgets\Breadcrumbs;
$this->title = 'Reservk | Crear cuenta';
?>
<head>
    <link rel="stylesheet" href="../../assets/styles/login3.css">

</head>
    <section>
        <div class="container" id="container">
            <div class="form-container sign-in-container" >
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
         
                <div> <input type="image" src="../../assets/images/logo-footer.png" class="imgh"> </div>

                <h1>Ingresar</h1>
                <div>
                    <div class="yol">
                        <?= $form->field($model, 'name')->textInput(['id' => 'name', 'autofocus' => true, 'class' => 'si', 'maxlength' => '20', 'minlength' => '5']) ?>

                    </div>
                    <div class="col"><?= $form->field($model, 'surname')->textInput(['id' => 'surname', 'class' => 'si', 'maxlength' => '20', 'minlength' => '5']) ?></div>

                    <div class="los"> <?= $form->field($model, 'id_number')->textInput(['id' => 'id_number', 'type' => 'text', 'class' => 'si', 'maxlength' => '10', 'minlength' => '10']) ?></div>
                </div>

                <div class="ñp"><?= $form->field($model, 'username')->textInput(['id' => 'username', 'type' => 'email', 'class' => 'si', 'maxlength' => '30', 'minlength' => '10', 'oncopy' => 'return false', 'onpaste' => 'return false']) ?></div>

                <div class="ig"> <?= $form->field($model, 'email')->textInput(['id' => 'email', 'type' => 'email', 'class' => 'si', 'maxlength' => '30', 'minlength' => '10', 'oncopy' => 'return false', 'onpaste' => 'return false']) ?></div>


                <div class="igs"><?= $form->field($model, 'password')->passwordInput(['id' => 'password', 'type' => 'password', 'id' => 'txtPassword', 'class' => 'sis', 'maxlength' => '12', 'minlength' => '8', 'oncopy' => 'return false', 'onpaste' => 'return false']) ?></div>

                <button class="ojo" onclick="mostrarPassword()"> <img src="../../assets/images/ojo(1).png" style="height: 24px;  width: 23px;"> <span class="fa fa-eye-slash icon"></span> </button>

                <div class="igss"> <?= $form->field($model, 'password2')->passwordInput(['id' => 'password2', 'class' => 'sis', 'maxlength' => '12', 'minlength' => '8', 'oncopy' => 'return false', 'onpaste' => 'return false']) ?></div>

                <h6>Al registrarse en Reservk aceptas nuestros <?php echo Html::a('términos y condiciones y política de datos',  Url::to(['condiciones']), ['target' => '_blank']); ?>.</h6><br>
               
                <div class="che">

                    <input type="checkbox" value="1" name="checkbox" id="checkbox" onclick="signup.disabled = !this.checked" ></div>
                <div class="ches"> <label>Acepto los terminos</label></div>

                <div class="form-group">
                    <?= Html::submitButton('Registrarse', ['id' => 'btn', 'class' => 'but', 'name' => 'signup' ,'disabled'=>'diabled']) ?>
                </div>
                <a class="buts" href="<?= Url::to(['site/']) ?>">Cancelar</a>
                <div>
<?= Breadcrumbs::widget([
'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= Alert::widget() ?>
</div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="overlay-container">
                <div class="overlay">
                
                    <div class="overlay-panel overlay-right">
                    <img src="../../assets/images/Logo-blanco.png">
                        <h1>Hola, amigo!</h1>
                        <p>Inicie sesión con su información personal.</p>
<br>
                        <button onclick="location.href='<?= Url::to(['site/login']) ?>'" class="ghost"><b>Iniciar sesion</b></button>
                    </div>

                </div>
            </div>
        </div>
    </section>
<footer>
        <div class="row footer_row">

            <div class="foot" class="copyright">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;
                <script>
                    document.write(new Date().getFullYear());
                </script> Reservk Todos los derechos reservados.
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </div>
        </div>
        <input type="image" src="../../assets/images/logo-footer.png" class="imgfoot"></div>

</footer>