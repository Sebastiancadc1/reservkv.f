<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reservk | Cambiar contraseña ';
?>
<head>
	<script src="../../assets/js/btnlogin.js"></script>
	<link rel="stylesheet" href="../../assets/styles/login4.css">
</head>


	<section>
		<div class="container" id="container">
			<!-- <div class="form-container sign-up-container">
					</div> -->
			<div class="form-container sign-in-container">
				<?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
				<div> <input type="image" src="../../assets/images/logo-footer.png" class="imgh"> </div>

				<h1>Cambiar contraseña</h1>
				<?= $form->field($model, 'password')->passwordInput(['id' => 'txtPassword',  'autofocus' => true, 'class' => 'si']) ?>
				<button class="ojo" onclick="mostrarPassword()"> <img src="../../assets/images/ojo(1).png" style="height: 24px;  width: 23px;"></button>
				<div class="form-group">
					<?= Html::submitButton('Cambiar', ['class' => 'si']) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
			<div class="overlay-container">
				<div class="overlay">
					<div class="overlay-panel overlay-left">
						<h1>S</h1>
						<p>Inicie sesión con su información personal</p>
						<button class="ghost" id="signIn" onclick="btns()">Iniciar sesión</button>
					</div>
					<div class="overlay-panel overlay-right">
					<img src="../../assets/images/Logo-blanco.png">
						<h1>Hola, amigo!</h1>
						<p>Llene el campo y cambie su contraseña</p>
					</div>

				</div>
			</div>
		</div>
	</section>
	<footer>

		<div class="row footer_row">

			<div class="foot" class="copyright">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;
				<script>
					document.write(new Date().getFullYear());
				</script> Todos los derechos reservados.
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</div>
		</div>
		<input type="image" src="../../assets/images/logo-footer.png" class="imgfoot">
		</div>

	</footer>