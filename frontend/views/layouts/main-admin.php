<?php

use yii\helpers\Html;
use yii\helpers\Url;

if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    //app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <!-- <title>RESERVK | Reservas</title> -->
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/img/core-img/favicon.ico">
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/calendar.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/calendario.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/Calendario/bootstrap-datetimepicker.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/sweetalert2.min.css']); ?>
    <?php $this->head() ?>
</head>
<style>
    .sidebar-collapse .fa-user-circle {
        font-size: 17px !important;
    }
</style>

<body class="hold-transition sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?=
            $this->render(
                'header.php',
                ['directoryAsset' => $directoryAsset]
            )
        ?>

        <?=
            $this->render(
                'left.php',
                ['directoryAsset' => $directoryAsset]
            )
        ?>

        <?=
            $this->render(
                'content.php',
                ['content' => $content, 'directoryAsset' => $directoryAsset]
            )
        ?>

    </div>

    <?php $this->endBody() ?>

</body>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/es-ES.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/moment.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap-datetimepicker.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap-datetimepicker.es.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/underscore-min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/calendar.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/sweetalert2.all.min.js"></script>

<script>
            $.ajaxSetup({
                data: <?=
                            \yii\helpers\Json::encode([
                                \yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
                            ])
                        ?>
            });
        </script>
</html>
<?php $this->endPage() ?>