<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="../assets/images/favicon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>

    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/bootstrap-4.1.2/bootstrap.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/colorbox/colorbox.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/main_styles.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/booking.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/booking_responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/contact.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/contact_responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/calendar.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/Calendario/bootstrap-datetimepicker.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/calendario.css']); ?>

    <title><?= Html::encode($this->title) ?></title>


    <?php $this->head() ?>
</head>

<body>

    <?php $this->beginBody() ?>
    <div>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
    <?php $this->endBody() ?>
</body>

<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/es-ES.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/moment.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap-datetimepicker.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap-datetimepicker.es.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/underscore-min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/calendar.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/ojo.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/ojo.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/btnlogin.js"></script>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

</html>


<?php $this->endPage() ?>