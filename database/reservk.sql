-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2020 a las 03:21:03
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reservk`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_ambiente` (IN `p_Id_ambiente` INT(11), IN `p_title` VARCHAR(14), IN `p_Objetos_del_ambiente` VARCHAR(45), IN `p_Cantidad_objetos` INT(11), IN `p_Lugar` VARCHAR(15), IN `p_Capacidad` VARCHAR(6))  NO SQL
INSERT INTO ambiente (`id_ambiente`, `title`, `objetos_del_ambiente`, `cantidad_objetos`,`capacidad`,`lugar`) 
VALUES
(p_id,p_title,p_Objetos_del_ambiente,p_Cantidad_objetos,p_Lugar,p_Capacidad)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_contra_recu` (IN `email` VARCHAR(30))  NO SQL
SELECT * FROM usuario WHERE Correo=email$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_dano_ambiente` (IN `p_Id_dano_ambiente` INT(11), IN `p_Detalle_del_dano` INT(45), IN `p_Tipo_de_dano` INT(200), IN `p_Objetos_del_ambiente` INT(45), IN `p_Cantidad_objetos` INT(11))  NO SQL
INSERT INTO dano_ambiente (`Id_dano_ambiente`, `Detalle_del_dano`, `Tipo_de_dano`, `Objetos_del_ambiente`, `Cantidad_objetos`) VALUES
(p_Id_dano_ambiente,p_Detalle_del_dano,p_Tipo_de_dano,p_Objetos_del_ambiente,p_Cantidad_objetos)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_edit_usu_activo` (IN `nik` INT)  NO SQL
UPDATE usuario set estado='activo' WHERE id_usuario= nik$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_edit_usu_inactivo` (IN `nik` INT)  NO SQL
UPDATE usuario set estado='inactivo' WHERE id_usuario=nik$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_eliminar_repor` (IN `p_Id_dano_ambiente` INT(11))  NO SQL
DELETE FROM `dano_ambiente` WHERE `Id_dano_ambiente`=p_Id_dano_ambiente$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_filactivo` (IN `filter` VARCHAR(10))  SELECT * FROM usuario WHERE estado = filter$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_insert_usu` (IN `name` VARCHAR(20), IN `lastname` VARCHAR(20), IN `cedu` INT(11), IN `tele` INT(11), IN `email` VARCHAR(30), IN `contra` VARCHAR(12))  NO SQL
INSERT INTO usuario (Nombre,Apellido,Cedula,Telefono,Correo,Tipo_Usuario,Contrasena,estado)
VALUES (name,lastname,cedu,tele,email,'Funcionario',contra,'activo')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_login` (IN `email` VARCHAR(30), IN `contra` VARCHAR(12))  NO SQL
SELECT * FROM usuario WHERE Correo = email AND Contrasena=contra AND estado='activo'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_mensaje` ()  NO SQL
SELECT * from usuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_modificar_dano` (IN `p_Id_dano_ambiente` INT(11), IN `p_Detalle_del_dano` VARCHAR(45), IN `p_Tipo_de_dano` VARCHAR(200), IN `p_Objetos_del_ambiente` VARCHAR(45), IN `p_Cantidad_objetos` INT(11))  NO SQL
UPDATE `dano_ambiente` SET `Id_dano_ambiente`=p_Id_dano_ambiente,`Detalle_del_dano`=p_Detalle_del_dano,`Tipo_de_dano`=p_Tipo_de_dano,`Objetos_del_ambiente`=p_Objetos_del_ambiente,`Cantidad_objetos`=p_Cantidad_objetos WHERE 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_reserva` (IN `p_id` INT(11), IN `p_start` DATETIME, IN `p_end` DATETIME, IN `p_descripcion` VARCHAR(20), IN `p_color` VARCHAR(20), IN `p_textColor` VARCHAR(20), IN `title` INT(11))  NO SQL
INSERT INTO `reserva`(`id`, `start`, `end`, `descripcion`, `color`,`textColor`,`Ambiente_id_ambiente`) VALUES
(p_id,p_start,p_end,p_descripcion,p_color,p_textColor,title)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_select_ambiente` ()  NO SQL
SELECT * FROM ambiente$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_select_correo_usu` (IN `email` VARCHAR(30))  NO SQL
SELECT * FROM usuario WHERE Correo = email$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_select_usuario` ()  NO SQL
SELECT * FROM usuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_usu` (IN `nik` INT)  NO SQL
SELECT * FROM usuario WHERE id_usuario = nik$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_v_consulta_danos` ()  NO SQL
SELECT * from v_consulta_danos$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_v_historial_danos` ()  NO SQL
SELECT * FROM v_historial_danos$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Pro_v_historial_reservas` ()  NO SQL
SELECT * FROM v_historial_reservas$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambiente`
--

CREATE TABLE `ambiente` (
  `id_ambiente` int(11) NOT NULL,
  `title` varchar(14) NOT NULL,
  `objetos_del_ambiente` varchar(45) NOT NULL,
  `cantidad_de_objetos` varchar(30) NOT NULL,
  `capacidad` varchar(6) NOT NULL,
  `Lugar` varchar(15) NOT NULL,
  `color` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`id_ambiente`, `title`, `objetos_del_ambiente`, `cantidad_de_objetos`, `capacidad`, `Lugar`, `color`) VALUES
(1, 'Gym', 'maquinas', '30 maquinas', '35', 'septimo piso', 'darkgren'),
(2, 'Auditorio', 'videovol,bafles,cabinas', '1videovol,8bafles,2cabina 	', '210', 'Segundo piso', 'black'),
(3, 'Audio visuales', ' parlantes,videovol', '6parlantes,1videovol', '45', 'Segundo piso', 'blue');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Rol/Permiso',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT 'Usuario',
  `created_at` datetime NOT NULL COMMENT 'Creado el'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('p-auth-all', 1, '2020-02-29 11:08:50'),
('p-auth-assignment-all', 1, '2020-02-29 11:02:21'),
('r-admin', 1, '2019-05-01 14:38:10'),
('r-admin', 33, '2020-02-29 15:31:08'),
('r-admin', 35, '2020-02-29 15:30:50'),
('r-admin', 39, '2020-02-29 21:19:51'),
('r-super-admin', 1, '2019-05-19 10:23:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Rol/Permiso',
  `type` int(11) NOT NULL COMMENT 'Tipo',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Descripción',
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nombre regla asociada',
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data',
  `created_at` datetime NOT NULL COMMENT 'Creado el',
  `updated_at` datetime NOT NULL COMMENT 'Actualizado el'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('p-auth-all', 2, 'Para que tenga acceso a todo el manejo de roles y permisos', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-auth-assignment-all', 2, 'Permite asignar roles y permisos a los usuarios', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-auth-item-all', 2, 'Tiene acceso CRUD de permisos y roles', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-auth-item-child-all', 2, 'Puede cambiar la jerarquía de permisos', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-auth-menu-item-all', 2, 'Puede organizar el menú de la aplicación', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-configuracion-all', 2, 'Permite configurar el sistema', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-menu-basico', 2, 'Sólo se usa para generar el menú', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-parametrizacion-avanzada-all', 2, 'Hacer labores de parametrización', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-parametrizacion-sistema-all', 2, 'Parametrización que puede hacer el coordinador', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('p-user-all', 2, 'Puede gestionar los usuarios', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('r-admin', 1, 'Encargado de realizar la administración del sistema', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('r-administrador-jagao', 1, 'Administrador de Jagao , puede hacer todo en el sistema menos ingresar a la administración de roles y permisos', NULL, NULL, '2019-05-04 16:34:36', '2019-05-04 16:34:36'),
('r-proveedor', 1, 'Rol para los usuarios que pueden hacer las veces de un proveedor', NULL, NULL, '2019-05-04 16:25:47', '2019-05-04 16:25:47'),
('r-prueba', 1, 'Rol de prueba', NULL, NULL, '2019-05-01 17:50:37', '2019-05-01 17:50:37'),
('r-solicitante-proveedor', 1, 'Rol para el usuario que realiza la solicitud de proveedor por primera vez.', NULL, NULL, '2019-05-04 22:59:13', '2019-05-04 22:59:13'),
('r-super-admin', 1, 'Es el todo poderoso del sistema.  Puede cambiar los roles y permisos.  ', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Rol/Permiso padre',
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Rol/Permiso hijo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('p-auth-all', 'p-auth-assignment-all'),
('p-auth-all', 'p-auth-item-all'),
('p-auth-all', 'p-auth-item-child-all'),
('p-auth-all', 'p-auth-menu-item-all'),
('r-admin', 'p-menu-basico'),
('r-admin', 'p-parametrizacion-sistema-all'),
('r-admin', 'p-user-all'),
('r-administrador-jagao', 'r-solicitante-proveedor'),
('r-super-admin', 'p-auth-all'),
('r-super-admin', 'p-configuracion-all'),
('r-super-admin', 'p-parametrizacion-avanzada-all'),
('r-super-admin', 'r-admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dano_ambiente`
--

CREATE TABLE `dano_ambiente` (
  `Id_dano_ambiente` int(11) NOT NULL,
  `Detalle_del_dano` varchar(45) NOT NULL,
  `Tipo_de_dano` varchar(200) NOT NULL,
  `Reserva_id_reserva` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dano_ambiente`
--

INSERT INTO `dano_ambiente` (`Id_dano_ambiente`, `Detalle_del_dano`, `Tipo_de_dano`, `Reserva_id_reserva`) VALUES
(13, 'Se daño una silla\r\n', 'Grave', 178),
(25, 'ts', 'ts', 226),
(26, 'ts', 'ts', 227),
(27, 'ts', 'ts', 228),
(28, 'f', 'f', 230);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `id` int(11) NOT NULL,
  `title` int(11) NOT NULL,
  `body` text NOT NULL,
  `type_event` varchar(50) NOT NULL,
  `url` varchar(150) NOT NULL,
  `class` varchar(45) NOT NULL,
  `start` varchar(30) NOT NULL,
  `end` varchar(30) NOT NULL,
  `inicio_normal` datetime NOT NULL,
  `final_normal` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `limit_events` int(11) NOT NULL DEFAULT 1,
  `usuario_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`id`, `title`, `body`, `type_event`, `url`, `class`, `start`, `end`, `inicio_normal`, `final_normal`, `status`, `limit_events`, `usuario_id`) VALUES
(178, 2, 'Capacitacion covid', 'event-info', '/reservk/frontend/web/descripcion_evento.php?id=178', 'cancel-event', '1585351380000', '1585351380000', '2020-03-27 18:23:00', '2020-03-27 18:23:00', 'cancelado', 1, 1),
(225, 1, 'a', 'event-info', '/reservk22/reservk/frontend/web/descripcion_evento.php?id=225', 'cancel-event', '1585347360000', '1585350960000', '2020-03-27 17:16:00', '2020-03-27 18:16:00', 'cancelado', 1, 1),
(226, 1, 'a', 'event-info', '/reservk22/reservk/frontend/web/descripcion_evento.php?id=226', 'event-especial', '1585428540000', '1585428540000', '2020-03-28 15:49:00', '2020-03-28 15:49:00', 'activo', 1, 1),
(227, 1, 'a', 'event-info', '/reservk22/reservk/frontend/web/descripcion_evento.php?id=227', 'event-especial', '1585514940000', '1585514940000', '2020-03-29 15:49:00', '2020-03-29 15:49:00', 'activo', 1, 1),
(228, 1, 'a', 'event-info', '/reservk22/reservk/frontend/web/descripcion_evento.php?id=228', 'event-especial', '1585601340000', '1585601340000', '2020-03-30 15:49:00', '2020-03-30 15:49:00', 'activo', 1, 1),
(229, 1, 'ts', 'event-info', '/reservk22/reservk/frontend/web/descripcion_evento.php?id=229', 'event-especial', '1585342260000', '1585342260000', '2020-03-27 15:51:00', '2020-03-27 15:51:00', 'activo', 1, 59),
(230, 2, 'asd', 'event-info', '/reservk22/reservk/frontend/web/descripcion_evento.php?id=230', 'cancel-event', '1585599420000', '1585599420000', '2020-03-30 15:17:00', '2020-03-30 15:17:00', 'cancelado', 1, 1),
(231, 2, 'f', 'event-info', '/fff/reservk/frontend/web/descripcion_evento.php?id=231', 'cancel-event', '1585432920000', '1585432920000', '2020-03-28 17:02:00', '2020-03-28 17:02:00', 'cancelado', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_usuarios`
--

CREATE TABLE `tipos_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Descripción del tipo de usuario',
  `permiso_rol` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Permiso o rol con el que esta asociado este tipo de usuario',
  `creado_por` int(10) UNSIGNED NOT NULL,
  `creado_el` datetime NOT NULL,
  `actualizado_por` int(10) UNSIGNED NOT NULL,
  `actualizado_el` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos_usuarios`
--

INSERT INTO `tipos_usuarios` (`id`, `descripcion`, `permiso_rol`, `creado_por`, `creado_el`, `actualizado_por`, `actualizado_el`) VALUES
(1, 'Administrativo', 'r-administrativo-empresa', 10, '2019-12-29 20:38:45', 10, '2019-12-29 20:38:45'),
(2, 'Operativo', 'r-conductor-empresa', 10, '2019-12-29 20:38:55', 10, '2019-12-29 20:38:55'),
(3, 'Administrativo', 'r-administrativo-empresa', 10, '2019-12-29 20:44:22', 10, '2019-12-29 20:44:22'),
(4, 'Administrativo', 'r-administrativo-empresa', 10, '2019-12-29 20:45:45', 10, '2019-12-29 20:45:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `id_number` int(11) NOT NULL COMMENT 'Número de identificación del usuario',
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombres',
  `surname` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Apellidos',
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombre de usuario',
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Correo personal',
  `estado` tinyint(1) NOT NULL COMMENT '¿Está activo?',
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creado_por` int(11) UNSIGNED DEFAULT NULL COMMENT 'Creado por',
  `created_at` datetime NOT NULL COMMENT 'Creado el',
  `actualizado_por` int(11) UNSIGNED DEFAULT NULL COMMENT 'Actualizado por',
  `updated_at` datetime NOT NULL COMMENT 'Modificado el'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `id_number`, `name`, `surname`, `username`, `email`, `estado`, `auth_key`, `password_hash`, `password_reset_token`, `verification_token`, `creado_por`, `created_at`, `actualizado_por`, `updated_at`) VALUES
(1, 1001279272, 'Sebastian', 'Rodriguez', 'sr928291@gmail.com', 'sr928291@gmail.com', 1, 'sLH6aXuJGolxMEo964MuqshYdEm56KuV', '$2y$13$4sMjO6eoCd7yjWhuWJY4E.DEa6ZELobAAc/ZQGf3oN3IA3DHIYJtu', 'No-EoYewZQjibAHFzj7w0vRVfEWUiw7m_1584143088', 'g_M7joOcjgzK9R0-CmnP3Aw3eqc4qBwK_1556729591', NULL, '2019-05-01 11:53:11', NULL, '2020-03-13 18:44:48'),
(54, 1001564789, 'Roberto', 'Gomez', 'azul.sebastian@hotmail.com', 'azul.sebastian@hotmail.com', 0, 'bwCex10SGJm_SLApoj_nqRQzcGIrYoJ9', '$2y$13$3JLdv3YGgVTasVCEJLyspexqZWRlo6AMBlRfwnyvfmAKcFDeQ5mCa', NULL, 'PmV4_pzN84vCW9vT4w_-Sk0jlT1zr-cV_1583624213', NULL, '2020-03-07 18:36:53', 1, '2020-03-13 19:12:49'),
(59, 1212312124, 'brandon', 'bautista', 'ambientes.re@gmail.com', 'ambientes.re@gmail.com', 1, 'I_L5WukWJkHHGGTJ7Ll7dXCP_nKu_JXK', '$2y$13$tJFNra4ZuhL4Q6UN.DoYfusxWBtfVMh6YI.nsLpGi5QtyQ7ptMMXS', NULL, 'ebTLxarILll6Ye76Ye_yUeDYZQTn_M0b_1584452135', NULL, '2020-03-17 08:35:35', 59, '2020-03-17 09:12:40');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`id_ambiente`),
  ADD KEY `ambiente` (`title`) USING BTREE;

--
-- Indices de la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD UNIQUE KEY `item_name_user_id` (`item_name`,`user_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`),
  ADD KEY `item_name` (`item_name`);

--
-- Indices de la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indices de la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD UNIQUE KEY `parent_child` (`parent`,`child`) USING BTREE,
  ADD KEY `child` (`child`),
  ADD KEY `parent` (`parent`);

--
-- Indices de la tabla `dano_ambiente`
--
ALTER TABLE `dano_ambiente`
  ADD PRIMARY KEY (`Id_dano_ambiente`),
  ADD KEY `Reserva_id_reserva` (`Reserva_id_reserva`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `Usuario_id_usuario_2` (`usuario_id`);

--
-- Indices de la tabla `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creado_por` (`creado_por`),
  ADD KEY `actualizado_por` (`actualizado_por`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `id_number` (`id_number`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `creado_por` (`creado_por`),
  ADD KEY `actualizado_por` (`actualizado_por`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  MODIFY `id_ambiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dano_ambiente`
--
ALTER TABLE `dano_ambiente`
  MODIFY `Id_dano_ambiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;

--
-- AUTO_INCREMENT de la tabla `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=60;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `dano_ambiente`
--
ALTER TABLE `dano_ambiente`
  ADD CONSTRAINT `dano_ambiente_ibfk_2` FOREIGN KEY (`Reserva_id_reserva`) REFERENCES `reserva` (`id`);

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reserva_ibfk_2` FOREIGN KEY (`title`) REFERENCES `ambiente` (`id_ambiente`),
  ADD CONSTRAINT `reserva_ibfk_3` FOREIGN KEY (`usuario_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
